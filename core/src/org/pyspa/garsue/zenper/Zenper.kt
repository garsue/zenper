package org.pyspa.garsue.zenper

import com.badlogic.gdx.Application
import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.badlogic.gdx.graphics.glutils.PixmapTextureData
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.utils.Base64Coder
import com.badlogic.gdx.utils.viewport.FitViewport


class Zenper() : ApplicationAdapter() {
    private companion object {
        private val GAME_WIDTH = 128
        private val GAME_HEIGHT = 128
        private val JUMP_HEIGHT = 12
        private val bg = Color(1f, 1f, 0xEE / 255f, 1f)
        private fun log(s: String) = Gdx.app.debug("zenper", s)
    }

    private val batch: Batch by lazy { SpriteBatch() }
    private val camera: OrthographicCamera by lazy { OrthographicCamera() }
    private val viewport: FitViewport by lazy { FitViewport(GAME_WIDTH.toFloat(), GAME_HEIGHT.toFloat(), camera) }
    private val texture: Texture by lazy {
        val img = "R0lGODlh2AEIAIABAAAAAP///yH5BAEKAAEALAAAAADYAQgAAAL+jI+py+0PYopA0SlD1Wjv11xYR31lZojnp6lrKsIxSNf2Eb+4m59yubugJLrRsILaKY8/j6/ZNPWAraW0CsNWM9zizbJifWmzltiMuY7GuGxQ6Yb/4qT11hRPsvc2ohPehRVoV1eopVc3iCjk98f0eAVJlySZBYnkRyfzFcYxwVdz5mmxIEVZ1qemuSl3F7VaqJInClprl2l5VmnYOtubiPnbCYTrmmuFXMqymxlspiiGCuKRMkptG6JLuxjZPcbUSzUnKNpYdooujY196QJmNeWO1gmbewrmM+x9zM9zL9doXpEZ4lhx+oRn1DpCHBBpsfTqmUMH4CapM4eqWEWoTwXrLQSlL5mhYsL2HaJ1p8e7YOZOSnTE6KIqlxKjOCEYbSKZNhyvfeTF890nmy9RZqs2Us9GciI3Lsv58yezP8YGAXWjcaYkeUnJOTMWCyZVmr4MxhSoVWeoIVijDu2gMGibmy+8rH1KTMeTpPGepjmi1u23bgWp7A2pjBG8vlgxZqX5xCExr1DCdfTLj2xRdmcFT5L70FrPNKE9mz6NOrXq1axbgyoAADs="
        val bytes = Base64Coder.decode(img)
        val pixmap = Pixmap(bytes, 0, bytes.size)
        Texture(PixmapTextureData(pixmap, Pixmap.Format.RGBA4444, false, true, false))
    }
    private val shape: ShapeRenderer by lazy { ShapeRenderer() }

    private var elapse = 0f
    private var renderCount = 0
    private var touchStart = 0
    private val touchPos = Vector2()

    private val player = Player()
    private val dragons = Array(8, {
        val d = Dragon()
        d.reset()
        d
    })
    private val pants = Array(12, {
        val p = Pants()
        p.reset()
        p
    })
    private val snows = Array(100, {
        val s = Snow()
        s.reset()
        s
    })
    val ground = Array(GAME_WIDTH + 1, { 0f })  // Ground height

    override fun create() {
        Gdx.app.logLevel = Application.LOG_DEBUG
    }

    override fun render() {
        camera.update()

        elapse += Gdx.graphics.deltaTime
        if (elapse * 1000 > 50) {
            elapse = 0f
            act()
        }

        batch.projectionMatrix = camera.combined
        shape.projectionMatrix = camera.combined

        Gdx.gl.glClearColor(0.4f, 0.4f, 0.4f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        shape.begin(ShapeRenderer.ShapeType.Filled)
        // Draw background
        shape.color = bg
        shape.rect(0f, 0f, GAME_WIDTH.toFloat(), GAME_HEIGHT.toFloat())
        // Draw life bar
        shape.color = Color.BLACK
        shape.rect(40f, GAME_HEIGHT - 8f * 2, player.life.toFloat(), 8f)
        // Draw snow
        for (s in snows) {
            val i = s.p.x.toInt()
            if (i < 0 || ground.size <= i) {
                continue
            }
            // Draw falling snow
            if (s.p.y >= ground[i]) {
                shape.point(s.p.x, s.p.y, 1f)
            }
        }
        // Draw ground
        let {
            var x1 = 0f
            var y1 = ground[0]
            for ((i, y2) in ground.drop(1).withIndex()) {
                val x2 = i.toFloat() + 1
                shape.line(x1, y1, x2, y2)
                x1 = x2
                y1 = y2
            }
        }
        shape.end()

        batch.begin()
        // Draw text
        drawString(batch, "SCORE " + player.score, 0f, GAME_HEIGHT - 8f)
        drawString(batch, "LIFE", 0f, GAME_HEIGHT - 8f * 2)
        if (player.isGameOver()) {
            drawString(batch, "GAME OVER", 24f, GAME_HEIGHT - 64f)
        }
        // Draw player
        drawRegion(batch, player.label, player.p.x, player.p.y)
        // Draw dragons
        for (d in dragons.filter { d -> d.walking }) {
            drawRegion(batch, d.label, d.p.x, d.p.y)
        }
        // Draw pants
        for (p in pants.filter { p -> p.falling }) {
            drawRegion(batch, p.label, p.p.x, p.p.y)
        }
        batch.end()

        renderCount++
    }

    override fun resize(width: Int, height: Int) {
        log("resize $width $height")
        viewport.update(width, height, true)
    }

    override fun dispose() {
        super.dispose()
        batch.dispose()
        texture.dispose()
        shape.dispose()
    }

    private fun getRegion(label: Int) = TextureRegion(texture, label * 8, 0, 8, 8)

    private fun drawRegion(batch: Batch, label: Int, x: Float, y: Float) {
        batch.draw(getRegion(label), x, y)
    }

    private fun drawString(batch: Batch, s: String, x: Float, y: Float) {
        for ((i, c) in s.withIndex()) {
            drawRegion(batch, c.toInt() - 32, x + i * 8, y)
        }
    }

    private fun act() {
        // Do nothing if game over
        if (player.isGameOver()) {
            return
        }
        // Update snow
        for (s in snows) {
            var i = MathUtils.clamp(s.p.x.toInt(), 0, GAME_WIDTH - 1)
            if (s.p.y < ground[i]) {
                // Update ground
                var base = (ground[i + 1] + s.p.y + 2) / 2
                base = if (base <= GAME_HEIGHT) base else 0f
                ground[i] = base
                ground[i + 1] = base
                s.reset()
            } else {
                s.p.x += s.v
                s.p.y--
                s.v *= 1f - 1.95f * if (s.p.y % 4 == 0f) 1f else 0f
            }
        }
        // Update player
        if (player.jump > 0) {
            player.p.y += player.jump - JUMP_HEIGHT / 2
            player.jump--
        } else {
            player.p.y = ground[player.p.x.toInt() + 4]
        }
        // Update dragons
        for (d in dragons) {
            if (d.walking) {
                val i = d.p.x.toInt()
                d.p.y = if (0 <= i && i < ground.size && ground[i] <= GAME_HEIGHT) ground[i] else d.p.y
                d.p.x--
                d.label = d.label xor 1
                if (player.hit(d.p)) {
                    log("hit $d $player")
                    player.life--
                }
                if (d.p.x < -8) {
                    d.reset()
                }
            } else if ((renderCount and 64 == 0) && MathUtils.randomBoolean(0.002f)) {
                log("Emerge $d")
                d.reset()
                d.walking = true
            }
        }
        // Update pants
        for (p in pants) {
            if (p.falling) {
                p.p.y--
                if (p.p.y <= 0) {
                    p.reset()
                } else if (player.hit(p.p)) {
                    player.score = Math.max(0, player.score + (300 - (p.label and 1) * 400))
                    log("hit $p $player")
                    p.reset()
                }
            } else if ((renderCount and 32 == 0) && MathUtils.randomBoolean(0.005f)) {
                log("Emerge $p")
                p.reset()
                p.falling = true
            }
        }

        handleInput()
    }

    private fun handleInput() {
        // Horizontal move
        val touched = Gdx.input.isTouched(0)
        if (touched) {
            touchPos.x = Gdx.input.getX(0).toFloat()
            touchPos.y = Gdx.input.getY(0).toFloat()
            viewport.unproject(touchPos)
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT) || touched && player.p.x + 4 > touchPos.x) {
            player.label = (player.label or 2) xor 1
            player.p.x = Math.max(0f, player.p.x - 1f)
        } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || touched) {
            player.label = (player.label and 9) xor 1
            player.p.x = Math.min(GAME_WIDTH - 8f, player.p.x + 1f)
        }

        // Jump
        if (Gdx.input.justTouched()) {
            touchStart = renderCount
        }
        val tapped = renderCount - touchStart < 5
        if ((Gdx.input.isKeyPressed(Input.Keys.UP) || tapped) && player.jump == 0) {
            player.jump = JUMP_HEIGHT
            player.p.y = ground[player.p.x.toInt()]
        }
    }

    private data class Player(val p: Vector2 = Vector2(), var life: Int = 64, var score: Int = 0) {
        var label = 8
        var jump = 0
        fun isGameOver() = this.life <= 0

        fun hit(v: Vector2): Boolean {
            val a = p.x + 8
            val b = p.y + 8
            if (!(v.x <= p.x && p.x < v.x + 8 || v.x <= a && a < v.x + 8)) {
                return false
            }
            if (!(v.y <= p.y && p.y < v.y + 8 || v.y <= b && b < v.y + 8)) {
                return false
            }
            return true
        }
    }

    private data class Dragon(val p: Vector2 = Vector2(), var label: Int = 12) {
        var walking = false

        fun reset() {
            p.x = GAME_WIDTH.toFloat()
            p.y = 8f
            walking = false
        }
    }

    private data class Pants(val p: Vector2 = Vector2(), var label: Int = 0) {
        var falling = false

        fun reset() {
            p.x = MathUtils.random(GAME_WIDTH).toFloat()
            p.y = GAME_HEIGHT.toFloat()
            label = MathUtils.random(2, 3)
            falling = false
        }
    }

    private data class Snow(val p: Vector2 = Vector2(), var v: Float = 1f) {
        fun reset() {
            p.x = MathUtils.random(GAME_WIDTH).toFloat()
            p.y = (MathUtils.random(GAME_HEIGHT) + GAME_HEIGHT).toFloat()
            v = 1f
        }
    }

}
