package org.pyspa.garsue.zenper.android

import android.os.Bundle
import com.badlogic.gdx.backends.android.AndroidApplication
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import org.pyspa.garsue.zenper.Zenper

public class AndroidLauncher : AndroidApplication() {
    override protected fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val config = AndroidApplicationConfiguration()
        initialize(Zenper(), config)
    }
}
